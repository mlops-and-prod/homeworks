---
jupyter: python3
---


```{python}
import contextily as cx
import geopandas as gpd
import pandas as pd
import plotly.express as px
import seaborn as sns
```

### 1. Data preview

```{python}
data = pd.read_csv("../data/2015-street-tree-census-tree-data.csv")
data.head(5)
```

```{python}
data.shape
```

```{python}
data.status.value_counts()
```

```{python}
data[data.status == 'Alive'].health.value_counts()
```

### 2. Nans in data

```{python}
nans = data.isna().sum(axis=0)
nans
```

```{python}
fig = px.pie(names=list(nans.index), values=list(nans.values))
fig.update_layout(
    height=600,
    hiddenlabels=list(nans[nans < 10_000].index),
    title="Amount of missed values in each column (if more than 10000)"
)
fig.show()
```

### 3. Pairplot

```{python}
sns.pairplot(data);
```

### 4. Correlation matrix between numeric features

```{python}
data.corr(method="pearson", numeric_only=True)
```

### 5. Trees distribution on New Yorks map

```{python}
gdf = gpd.GeoDataFrame(
    data, geometry=gpd.points_from_xy(data.longitude, data.latitude), crs="EPSG:4326"
)
```

```{python}
ax = gdf.plot(markersize=0.1, figsize=(8, 8));
ax.set_title("Distribution of trees in New York")
cx.add_basemap(ax, crs=gdf.crs);
```
