import argparse
import os

import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split


def preprocessing(data: pd.DataFrame, test_size: float) -> pd.DataFrame:
    """Preprocessing function that drops some columns, Nan values and splits data on train and test

    Args:
        data (pd.DataFrame): dataset
        test_size (float): Test size of data in range(0, 1)

    Returns:
        pd.DataFrame: preprocessed dataset
    """

    data = data.copy()
    data.drop(
        columns=["PassengerId", "Name", "Ticket", "Cabin", "Embarked"], inplace=True
    )
    data["Sex"] = data["Sex"].map({"male": 0, "female": 1})
    data["Age"] = np.trunc(data["Age"])
    # data = data.fillna(data.median())
    data.dropna(inplace=True)
    train_data, test_data = train_test_split(
        data,
        test_size=test_size,
        stratify=data["Survived"],
        random_state=42,
    )
    train_data["split_type"] = "train"
    test_data["split_type"] = "test"

    return pd.concat((train_data, test_data), ignore_index=True)


def save_data(data: pd.DataFrame, old_filepath: str, test_size: float) -> None:
    """Saving data to old_filepath's/preprocessed/ directory.

    Args:
        data (pd.DataFrame): preprocessed dataset
        old_filepath (str): filepath of original data, new data will be saved in the same directory
        test_size (float): Test size of data in range(0, 1)
    """

    dir_, filename = old_filepath.rsplit("/", 1)
    new_dir = os.path.join(dir_, "preprocessed")
    new_filename = f"dataset__test_size_{test_size}.csv"
    os.makedirs(new_dir, exist_ok=True)

    data.to_csv(os.path.join(new_dir, new_filename), index=False)


def main(filepath: str, test_size: float):
    data = pd.read_csv(filepath)
    data = preprocessing(data, test_size)
    save_data(data, filepath, test_size)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--filepath", type=str)
    parser.add_argument("--test_size", type=float)
    args = parser.parse_args()
    kwargs = vars(args)

    main(**kwargs)
