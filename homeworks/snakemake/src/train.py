import argparse
import datetime
import os
import pickle
from typing import Tuple, Union

import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import roc_auc_score
from sklearn.tree import DecisionTreeClassifier

Classifier = Union[DecisionTreeClassifier, RandomForestClassifier]


def read_data(csv_path: str) -> Tuple[pd.DataFrame, pd.Series, pd.DataFrame, pd.Series]:
    """Reading csv and splitting data on train/test features and target matrixes.

    Args:
        csv_path (str): Path of csv file to read.

    Returns:
        Tuple[pd.DataFrame, pd.Series, pd.DataFrame, pd.Series]:
            (train_features, train_target, test_features, test_target)
    """

    data = pd.read_csv(csv_path)
    train_data = data[data["split_type"] == "train"]
    test_data = data[data["split_type"] == "test"]

    X_train = train_data.drop(columns=["Survived", "split_type"], axis=1)
    y_train = train_data["Survived"]

    X_test = test_data.drop(columns=["Survived", "split_type"], axis=1)
    y_test = test_data["Survived"]

    return (X_train, y_train, X_test, y_test)


def init_and_fit(
    model_type: str, features: pd.DataFrame, target: pd.Series
) -> Classifier:
    """Initializes the model and train it on given data

    Args:
        model_type (str): Model name
        features (pd.DataFrame): Train features matrix
        target (pd.Series): Train target vector

    Raises:
        NotImplemented: If incorrect model_type, available: ["tree", "forest"]

    Returns:
        Union[DecisionTreeClassifier, RandomForestClassifier]: Trained model
    """

    model_type = model_type.lower()
    model = None
    if "tree" in model_type:
        model = DecisionTreeClassifier(random_state=42)
    elif "forest" in model_type:
        model = RandomForestClassifier(random_state=42)
    else:
        raise ValueError("Available model_type: [tree, forest].")

    model.fit(features, target)

    return model


def evaluate(
    model: Classifier, test_features: pd.DataFrame, test_target: pd.Series
) -> Tuple[str, float]:
    """Evaluates the model on test data, computing metric.

    Args:
        model (Classifier): Trained model to evaluate
        test_features (pd.DataFrame): Test features matrix
        test_target (pd.Series): Test target matrix

    Returns:
        Tuple[str, float]: Tuple with metric name and score.
    """

    preds = model.predict(test_features)
    metric = round(roc_auc_score(test_target, preds), 4)
    return ("roc_auc", metric)


def save_artifacts(
    model: Classifier, model_type: str, suffix: str, output_dir: str = "artifacts"
) -> None:
    """Saving the trained model.

    Args:
        model (Classifier): Trained model
        model_type (str): Model type either "tree" or "forest".
        suffix (str): suffix that will be added to models name
        output_dir (str, optional): Where to save the model. Defaults to "artifacts".
    """

    os.makedirs(output_dir, exist_ok=True)

    path = os.path.join(output_dir, f"{model_type}__{suffix}.pkl")

    with open(path, "wb") as file:
        pickle.dump(model, file)


def main(model_type: str, csv: str, test_size: float):
    X_train, y_train, X_test, y_test = read_data(csv)
    model = init_and_fit(model_type, X_train, y_train)
    test_metric = evaluate(model, X_test, y_test)
    print(test_metric)
    save_artifacts(model, model_type, suffix=f"ts_{test_size}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--csv", type=str, required=True)
    parser.add_argument("--model_type", type=str, default="tree")
    parser.add_argument("--test_size", type=float, required=True)
    # parser.add_argument("--output_dir", type=str, default="artifacts")

    args = parser.parse_args()
    kwargs = vars(args)

    main(**kwargs)
