# Snakemake

## Installation

1. Если не установлена mamba, то ее можно установить так:
```bash
conda install -c conda-forge mamba
```
2. Создание mamba окружения:
```bash
mamba init
mamba env create -f env.yml
mamba activate snakemake
```
3. Не забыть прописать свои kaggle username и token в файле `~/.kaggle/kaggle.json`:
```json
{
    "username": "<username>",
    "key": "<token>"
}
```

## Rules

В файле [Snakefile](./Snakefile) прописан пайплайн обработки датасета и обучения моделей.

Он состоит из следующих правил:

1. **download_dataset** - скачивает датасет с kaggle и сохраняет его в `data/dataset.csv`
2. **preprocess** - делает предобработку скачанного датасета и сохраняет в `data/preprocessed/dataset__test_size_{test_size}.csv`, где `{test_size}` - это wildcard параметр.
3. **train_model** - обучает выбранную модель на предобработанных данных и сохраняет веса модели в `artifacts/{model_type}__ts_{test_size}.pkl`, где `{model_type}` и `{test_size}` - это wildcard параметры.

## Run

Для запуска snakemake нужно прописать следующее:
```bash
snakemake --cores 4
```
где `--cores` - кол-во ядер для запуска пайплайна, в примере их 4.

Визуализация пайплайна, граф будет сохранен в файл `dag.svg`:
```bash
snakemake --cores 4 --dag | dot -Tsvg > assets/dag.svg
```
Для текущего Snakefile'a dag выглядит следующим образом:

![dag](./assets/dag.svg)
