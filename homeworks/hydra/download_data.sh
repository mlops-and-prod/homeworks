mamba run -n hydra kaggle competitions download -c titanic -p data/ && \
    unzip data/titanic.zip -d data/ && \
    mv data/train.csv data/dataset.csv && \
    rm data/titanic.zip data/gender_submission.csv data/test.csv
