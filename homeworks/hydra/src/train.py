import os

import hydra
from hydra.utils import instantiate
from loguru import logger
from omegaconf import DictConfig, OmegaConf
from utils import evaluate, preprocess_data, save_artifacts


@hydra.main(config_path="../configs/", config_name="train_forest.yaml", version_base=None)
def main(cfg: DictConfig):
    cfg.model.n_jobs = cfg.n_jobs // 2  # limit cpu cores

    logger.add(os.path.join(cfg.logs_path, "output.log"), enqueue=True)
    logger.info(f"Train config:\n{OmegaConf.to_yaml(cfg)}")

    scaler = instantiate(cfg.preprocessing)
    model = instantiate(cfg.model)

    (X_train, y_train, X_test, y_test) = preprocess_data(
        csv_path=cfg.csv_path,
        test_size=cfg.test_size,
        scaler=scaler,
        random_state=cfg.random_state,
    )
    logger.info("Data was preprocessed.")

    model.fit(X_train, y_train)
    logger.info("Model was trained.")

    metrics = evaluate(model, X_test, y_test)
    logger.info(f"Model was evaluated, test metrics: {metrics}")

    save_artifacts(model, metrics, cfg.logs_path)
    logger.info(f"Model and metrics were saved to: {cfg.logs_path}")


if __name__ == "__main__":
    main()
