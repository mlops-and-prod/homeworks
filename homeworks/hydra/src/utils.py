import json
import os
import pickle
from typing import Any, Dict, Optional, Tuple, Union

import numpy as np
import pandas as pd
from sklearn.metrics import roc_auc_score
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler, StandardScaler

Scaler = Union[MinMaxScaler, StandardScaler]


def preprocess_data(
    csv_path: str,
    test_size: float = 0.2,
    scaler: Optional[Scaler] = None,
    random_state: int = 42,
) -> Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
    """Reading csv and splitting data on train/test features and target matrixes.

    Args:
        csv_path (str): Path to csv file to read.
        test_size (float, optional): Test size, in range (0, 1). Defaults to 0.2.
        scaler (Optional[Scaler], optional): Sklearn scaler to transform data. Defaults to None.
        random_state (int, optional): Random seed. Defaults to 42.

    Returns:
        Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
            (train_features, train_target, test_features, test_target)
    """

    data = pd.read_csv(csv_path)
    data.drop(
        columns=["PassengerId", "Name", "Ticket", "Cabin", "Embarked"], inplace=True
    )
    data["Sex"] = data["Sex"].map({"male": 0, "female": 1})
    data["Age"] = np.trunc(data["Age"])
    # data = data.fillna(data.median())
    data.dropna(inplace=True)
    train_data, test_data = train_test_split(
        data,
        test_size=test_size,
        stratify=data["Survived"],
        random_state=random_state,
    )

    X_train = train_data.drop(columns=["Survived"], axis=1).to_numpy()
    y_train = train_data["Survived"].to_numpy()

    X_test = test_data.drop(columns=["Survived"], axis=1).to_numpy()
    y_test = test_data["Survived"].to_numpy()

    if scaler is not None:
        X_train = scaler.fit_transform(X_train)
        X_test = scaler.transform(X_test)

    return (X_train, y_train, X_test, y_test)


def evaluate(
    model: Any,
    test_features: np.ndarray,
    test_target: np.ndarray,
) -> Dict[str, float]:
    """Evaluates the model on test data, computing metric.

    Args:
        model (Any): Trained sklearn model to evaluate
        test_features (np.ndarray): Test features matrix
        test_target (np.ndarray): Test target matrix

    Returns:
        Dict[str, float]: Dictionary with (metric_name: score).
    """

    preds = model.predict(test_features)
    metric = round(roc_auc_score(test_target, preds), 4)
    return {"roc_auc": metric}


def save_artifacts(
    model: Any,
    metrics: Dict[str, float],
    output_dir: str,
) -> None:
    """Saving the trained model and metrics.

    Args:
        model (Any): Trained sklearn model
        metrics (Dict[str, float]): Dictionary with (metric_name: score).
        output_dir (str): Where to save the model.
    """

    os.makedirs(os.path.join(output_dir, "weights"), exist_ok=True)
    metric_name, score = list(metrics.items())[0]
    path = os.path.join(output_dir, "weights", f"model__{metric_name}_{score}.pkl")

    with open(path, "wb") as file:
        pickle.dump(model, file)

    with open(os.path.join(output_dir, "metrics.json"), "w") as file:
        json.dump(metrics, file, indent=4)
