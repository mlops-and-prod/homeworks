# Hydra

## Installation

1. Если не установлена mamba, то ее можно установить так:
```bash
conda install -c conda-forge mamba
```
2. Создание mamba окружения:
```bash
mamba init
mamba env create -f env.yml
mamba activate hydra
```
3. Если датасет не скачан, не забыть прописать свои kaggle username и token в файле `~/.kaggle/kaggle.json`:
```json
{
    "username": "<username>",
    "key": "<token>"
}
```

## Download data

Для скачивания датасета необходимо запустить следующий скрипт:
```bash
bash download_data.sh
```
Датасет будет сохранен в текущей директории в `data/dataset.csv`

## Environment variables

Перед запуском обучения нужно задать переменную окружения `NUM_JOBS`, ограничивающую допустимое кол-во используемых ядер процессора для обучения модели:

```bash
export NUM_JOBS=4
```

либо задавать переменную уже при запуске самого скрипта:
```bash
NUM_JOBS=4 python src/...
```

## Train

В `configs/` лежат конфиги для обучения разных моделей.
Далее нужно запустить `src/train.py` указав параметром `--config-name` название нужного конфига. Также можно указать `--config-dir`, если он отличается от дефолтного `configs/`:

```python
python src/train.py --config-name train_forest
```
