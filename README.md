# Homeworks

## Build and run
1. Нужно собрать docker образ:
```bash
make build-image
```
2. Запустить docker контейнер:
```bash
make run-container
```

## Contributing
### Workflow
Данный репозиторий ведется по методологии `Github flow`:
 - Главная ветка - `main`.
 - Новые фичи разрабатываются в отдельных ветках.
 - Затем при необходимости делается `Merge Request` для запроса на слияние новой ветки с главной веткой `main`.
 - Проводится `Code Review` нового кода, все замечания указываются в самом `Merge Request'e` на `gitlab'e`.
 - После `Code Review`, если не нужно больше ничего исправлять, выполняется `Merge`.
 - Если новая ветка слита с `main`, после этого ее можно удалить.

Подробно про настройку среды для разработки: [CONTRIBUTING.md](./CONTRIBUTING.md)
