FROM ubuntu:22.04

ARG ENV_NAME=mlops-prod
ENV ENV_NAME=$ENV_NAME

SHELL ["/bin/bash", "-c"]

WORKDIR /work/

ENV TZ Europe/Moscow

RUN apt-get update && \
    apt-get install --yes --no-install-recommends curl wget make unzip nano

RUN wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O miniconda3.sh --no-check-certificate && \
    bash miniconda3.sh -b -p /miniconda3

ENV PATH=/miniconda3/bin:$PATH
RUN echo ". /miniconda3/etc/profile.d/conda.sh" >> ~/.profile && \
    conda init bash && \
    rm miniconda3.sh

COPY environments/dev.yml /tmp/
COPY pyproject.toml .
RUN conda env create -f /tmp/dev.yml
RUN echo "conda activate $ENV_NAME" >> ~/.bashrc

RUN conda run -n $ENV_NAME poetry config --local cache-dir ~/.poetry/cache && \
    conda run -n $ENV_NAME poetry install
