# Contributing
## Environment
### Installation
В данном проекте используется пакетный менеджер miniconda, его установка описана на [офф.сайте](https://docs.anaconda.com/free/miniconda/).

Все необходимые пакеты для разработки прописаны в [environments/dev.yml](./environments/dev.yml).
1. Для создания новой conda среды со всеми необходимыми зависимостями нужно запустить следующую команду:
```bash
make install-env
```

2. Далее активировать dev среду:
```bash
conda activate mlops-dev
```

3. Далее нужно инициализировать `pre-commit`:
```bash
pre-commit install
```

### New dependencies
Новые зависимости можно добавлять с помощью `conda` либо `pip`.

Далее их нужно зафиксировать в [environments/dev.yml](./environments/dev.yml) файле следующей командой:
```bash
make export-env
```

Для prod среды нужно скопировать новые зависимости из [environments/dev.yml](./environments/dev.yml) в [environments/prod.yml](./environments/prod.yml).

## Code linters
Для поддержания чистоты кода используются следующие линтеры и форматтеры:
 - `isort` - для проверки и исправления порядка импортируемых в каждом файле библиотек и модулей.
 - `black` - для проверки и исправления кода под стандарт PEP-8.
 - `mypy` - для нахождения ошибок несоответствия типов в коде.


Для применения линтеров и форматтеров:
```bash
make lint
```
**Также во время commit'a код прогоняется через линтеры и форматтеры автоматически, благодаря pre-commit'у, и в случае несоответствия кода появятся warning'и, и некоторые несоответствия форматтеры исправляют сами.**

## Configuration

Файл конфигурации `pre-commit'a`: [.pre-commit-config.yaml](./.pre-commit-config.yaml). В нем прописаны используемые линтеры и форматтеры.

Настройки самих линтеров и форматтеров прописаны в [pyproject.toml](./pyproject.toml) файле.

## Gitlab CI

Этапы в CI:

1. `docker` - Сборка docker образа и его публикация в Container Registry gitlab'a.
2. `linting` - Проверка кода с помощью линтеров.
3. `build` - Сборка и публикация python пакета в Package Registry.
4. `pages` - Рендер документации с помощью quarto.
