SHELL:=/bin/bash

CONDA_ACTIVATE = source $$(conda info --base)/etc/profile.d/conda.sh ; conda activate

ENV_NAME ?= mlops-dev


build-image:
	docker build -t ${ENV_NAME} --build-arg ENV_NAME=${ENV_NAME} .

run-container:
	docker run --gpus all -it --rm \
	-v ${PWD}:/work/ \
	--name=${ENV_NAME}-1 --ipc=host -it ${ENV_NAME}:latest


lint:
	$(CONDA_ACTIVATE) ${ENV_NAME} && \
	pre-commit run --all-files


install-env:
	conda env create -f environments/dev.yml

remove-env:
	conda remove -n ${ENV_NAME} --all

export-env:
	$(CONDA_ACTIVATE) ${ENV_NAME} && \
	conda env export | head -n -1 > environments/dev.yml
